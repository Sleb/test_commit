.SUFFIXES:
.PHONY: all clean fclean re

###############################
# Fill with your informations #
###############################

NAME = test.exe
CFLAGS = -Wall -Werror -Wextra

SOURCESFOLD = ./
HEADERSFOLD = ./
OBJECTSFOLD = ./objs

SOURCEFILES = test.c

###############
# End filling #
###############

SOURCES = $(patsubst %.c, $(SOURCESFOLD)/%.c, $(SOURCEFILES))
OBJECTFILES = $(SOURCEFILES:.c=.o)
OBJECTS = $(patsubst %.o, $(OBJECTSFOLD)/%.o, $(OBJECTFILES))

all: $(NAME)

$(NAME): $(OBJECTS)
	gcc -o $(NAME) $(OBJECTS)

$(OBJECTSFOLD)/%.o: $(SOURCESFOLD)/%.c
	gcc -c $^ -I$(HEADERSFOLD) -o $@
    
fclean: clean
	/bin/rm -f $(NAME)

clean:
	/bin/rm -f $(OBJECTS)

re: fclean all
