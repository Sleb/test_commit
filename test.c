#include    <stddef.h>
#include    <unistd.h>

size_t  ft_strlen(char *p_str)
{
    size_t  ret;

    ret = 0;
    if (p_str != NULL)
    {
        while (*p_str != '\0')
        {
            ret++;
            p_str++;
        }
    }

    return (ret);
}

void    ft_putchar(char c)
{
    write(1, &c, 1);
}

void    ft_putstr(char *p_str)
{
    if (p_str != NULL)
    {
        write(1, p_str, ft_strlen(p_str));
    }
}

void    ft_putendl(char *p_str)
{
    ft_putstr(p_str);
    ft_putchar('\n');
}

int     main(void)
{
    ft_putendl("Pouet");
    return (0);
}
